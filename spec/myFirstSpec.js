describe("myFirstSpec", function() {
  var myFunction;

  beforeEach(function() {
    // myModule = new MyModule();
  });

  describe("MyFunction", function() {
    it("should be a function", function() {
      expect(typeof MyFunction).toEqual("function")
    });

    it("should take two arguments that are numbers", function() {
      expect(function() {MyFunction(2, 3)}).not.toThrow();
    })

    it("should return the sum of those two numbers", function() {
      expect(MyFunction(2,3)).toEqual(5);
      expect(MyFunction(3,3)).toEqual(6);
    })

  });
});